<?php

namespace App\Service;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\JsonResponse;

class OrganisationManager
{

    // get All Organizations
    public function getOrganizations()
    {
        $organizations = Yaml::parseFile(dirname(__DIR__, 2) . '/var/organizations.yaml');

        return $organizations;
    }

    // Update Organization
    public function updateOrganisation($request, int $id): JsonResponse
    {
        $path = dirname(__DIR__, 2) . '/var/organizations.yaml';

        $organizations = $this->getOrganizations();

        if ($organizations['organizations'][$id]['name'] != $request->request->get('name')) {
            $organizations['organizations'][$id]['name'] = $request->request->get('name');
        }

        if ($organizations['organizations'][$id]['description'] != $request->request->get('description')) {
            $organizations['organizations'][$id]['description'] = $request->request->get('description');
        }

        $yaml = Yaml::dump($organizations);

        file_put_contents($path, $yaml);

        return new JsonResponse([
            'seccuce' => true
        ]);
    }

    // store Organisation
    public function storeOrganisation($request): JsonResponse
    {
        $path = dirname(__DIR__, 2) . '/var/organizations.yaml';
        $organizations = Yaml::parseFile($path);


        $organization['name'] = $request->request->get('name');

        $organization['description'] = $request->request->get('description');


        array_push($organizations['organizations'], $organization);

        $yaml = Yaml::dump($organizations);

        file_put_contents($path, $yaml);

        return new JsonResponse([
            'seccuce' => true
        ]);
    }

    // delete Organisation
    public function deleteOrganisation($id): JsonResponse
    {
        $path = dirname(__DIR__, 2) . '/var/organizations.yaml';
        $organizations = Yaml::parseFile($path);

        //delete item from array 
        unset($organizations['organizations'][$id]);

        $yaml = Yaml::dump($organizations);

        file_put_contents($path, $yaml);

        return new JsonResponse([
            'seccuce' => true
        ]);
    }
}
