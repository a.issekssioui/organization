<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\OrganisationManager;
use Symfony\Component\HttpFoundation\Request;

class OrganizationController extends AbstractController
{

    public function __construct(OrganisationManager $organisationManager)
    {
        $this->organisationManager = $organisationManager;
    }

    /**
     * @Route("/", name="organization")
     */
    public function index(): Response
    {

        $organizations = $this->organisationManager->getOrganizations();

        return $this->render('organization/index.html.twig', [
            'organizations' => $organizations['organizations'],
        ]);
    }

    /**
     * @Route("/edit_organisation/{id}", name="edit_organisation")
     */
    public function edit($id): Response
    {

        $organizations = $this->organisationManager->getOrganizations();

        return $this->render('organization/edit.html.twig', [
            'organization' => $organizations['organizations'][$id],
            'id' => $id
        ]);
    }

    /**
     * @Route("/add_organisation", name="add_organisation")
     */
    public function add_organisation()
    {
        return $this->render('organization/edit.html.twig');
    }

    /**
     * @Route("/update_organisation/{id}", name="update_organisation")
     */
    public function update(Request $request, $id): Response
    {

        try {

            $this->organisationManager->updateOrganisation($request, $id);
        } catch (\Exception $e) {

            error_log($e->getMessage());
        }

        return $this->redirect('/');
    }

    /**
     * @Route("/store_organisation/", name="store_organisation")
     */
    public function store(Request $request): Response
    {

        try {

            $this->organisationManager->storeOrganisation($request);
        } catch (\Exception $e) {

            error_log($e->getMessage());
        }

        return $this->redirect('/');
    }

    /**
     * @Route("/delete_organisation/{id}", name="delete_organisation")
     */
    public function delete($id): Response
    {

        try {

            $this->organisationManager->deleteOrganisation($id);
        } catch (\Exception $e) {

            error_log($e->getMessage());
        }

        return $this->redirect('/');
    }
}
